static void callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
         const GLchar *message, const void *userParam)
{
    assert(source == GL_DEBUG_SOURCE_SHADER_COMPILER);
    assert(type == GL_DEBUG_TYPE_OTHER);
    assert(severity == GL_DEBUG_SEVERITY_NOTIFICATION);

    const char* shader_name = userParam;
    printf("%s - %s\n", shader_name, message);
}

void init_debug(char* shader_name) {
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_FALSE);
	glDebugMessageControl(GL_DEBUG_SOURCE_SHADER_COMPILER, GL_DEBUG_TYPE_OTHER,
						  GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_TRUE);
	glDebugMessageCallback(callback, shader_name);
}
