#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#ifdef GLUT
#include "glut_backend.h"
#else
#include "gbm_backend.h"
#endif

GLuint compileShader(const char *shader_name, const char *sourceCode, GLsizei len) {
	char *file_extention = strrchr(shader_name, '.');
	GLenum shader_type;
	bool spirv;

	if (file_extention == NULL){
		fprintf(stderr, "ERROR: File type not specified\n");
		return 0;
	} else if (!strcmp(file_extention, ".vert")) {
		shader_type = GL_VERTEX_SHADER;
	} else if (!strcmp(file_extention, ".frag")) {
		shader_type = GL_FRAGMENT_SHADER;
	} else if (!strcmp(file_extention, ".spv")) {
		if (!strncmp(file_extention - 4, "vert", 4))
			shader_type = GL_VERTEX_SHADER;
		if (!strncmp(file_extention - 4, "frag", 4))
			shader_type = GL_FRAGMENT_SHADER;
		spirv = true;
	} else {
		fprintf(stderr, "ERROR: format '%s' not valid\n", file_extention);
		return 0;
	}

	// Create the shader
	GLuint shader = glCreateShader(shader_type);
	if (!shader)
		return shader;

	if (spirv) {
		glShaderBinary(1, &shader, GL_SHADER_BINARY_FORMAT_SPIR_V_ARB, sourceCode, len);
		gl_specialize_shader(shader, "main", 0, 0, 0);
	} else {
		// Pass the shader source code
		glShaderSource(shader, 1, &sourceCode, &len);
		// Compile the shader source code
		glCompileShader(shader);
	}

	// Check the status of compilation
	GLint status = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status) {
		// Get the info log for compilation failure
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen) {
			char* buf = (char*) malloc(infoLen);
			if (buf) {
				glGetShaderInfoLog(shader, infoLen, NULL, buf);
				fprintf(stderr, "Could not compile shader: %s\n", buf);
				free(buf);
			}
			// Delete the shader program
			glDeleteShader(shader);
			shader = 0;
		}
	}

	return shader;
}

int linkShader(GLuint program) {
	int status, loglen;
	char *buf;

	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (loglen > 0 && (buf = malloc(loglen + 1))) {
		glGetProgramInfoLog(program, loglen, 0, buf);
		buf[loglen] = '\0';
		printf("%s\n", buf);
		free(buf);
	}

	return status ? 0 : -1;
}

int main(int argc, char **argv) {
	int ret = -1, shader_size = 0;
	GLuint gl_shader, gl_program;
	FILE *shader_file;
	char *shader_code;

	shader_file = fopen(argv[1], "r");
	if (shader_file < 0) {
		fprintf(stderr, "ERROR: Couldn't open %s\n", argv[1]);
		goto out;
	}

	fseek(shader_file, 0, SEEK_END);
	shader_size = ftell(shader_file);
	rewind(shader_file);

	if (!(shader_code = malloc(shader_size + 1))) {
		fprintf(stderr, "failed to allocate %d bytes\n", shader_size + 1);
		goto malloc_error;
	}

	if (fread(shader_code, 1, shader_size, shader_file) < shader_size) {
		fprintf(stderr, "failed to read the shader: %s\n", argv[1]);
		goto fread_error;
	}
	shader_code[shader_size] = '\0';

	if (init(shader_code, shader_size, argv[1])) {
		fprintf(stderr, "Init backend fail\n");
		goto fread_error;
	}

	/*
	 * Compile shader
	 */
	printf("Renderer: %s\n",glGetString(GL_RENDERER));
	printf("Vendor: %s\n",glGetString(GL_VENDOR));
	fflush(stdout);
	gl_shader = compileShader(argv[1], shader_code, shader_size);
	if (gl_shader == 0) {
		fprintf(stderr, "ERROR: failed to compile the shader\n");
		goto backend_cleanup;
	}

	/*
	 * Create a gl program and attach the shader to it
	 */
	gl_program = glCreateProgram();
	if (gl_program == 0) {
		fprintf(stderr, "ERROR: Fail while creating the gl_program\n");
		goto backend_cleanup;
	}

	glProgramParameteri(gl_program, GL_PROGRAM_SEPARABLE, GL_TRUE);

	glAttachShader(gl_program, gl_shader);
	ret = linkShader(gl_program);

detach_shader:
	glDetachShader(gl_program, gl_shader);
destroy_shader:
	glDeleteShader(gl_shader);
delete_program:
	glDeleteProgram(gl_program);
backend_cleanup:
	cleanup();
fread_error:
	free(shader_code);
malloc_error:
	fclose(shader_file);
out:
	fflush(stderr);
	return ret;
}

