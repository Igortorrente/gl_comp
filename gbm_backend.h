#ifndef GDB_BACKEND
#define GDB_BACKEND

#include <epoxy/gl.h>
#include <epoxy/egl.h>
#include <GL/glx.h>

#define init(shader_code, shader_size, shader_name) gbmInit(shader_code, shader_size, shader_name)
#define cleanup() gbmCleanup()
#define gl_specialize_shader glSpecializeShaderARB

enum shader_type {
	TYPE_NONE,
	TYPE_CORE,
	TYPE_COMPAT,
	TYPE_ES,
	TYPE_VP,
	TYPE_FP,
};


int gbmInit(char* shader_code, int shader_size, char* shader_name);
void gbmCleanup();

#endif //GBM_BACKEND
