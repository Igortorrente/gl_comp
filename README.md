# gl_comp

A modified and simplified(in some ways) version of shader-db for a specific purpose. This program load, compile, and link one shader. Reporting the same information as in shader-db.
GLSL and SPIR-V are supported.

## Current limitations
- This program only accept one shader each time.
- Only Fragment and vertex shaders are accepted

## Dependencies

GCC libepoxy-dev libgbm-dev freeglut3-dev

## Compiling

### With gbm "backend"
``` sh
make
```

### With glut "backend"
``` sh
make glut
```

## How to use it
``` sh
./gl_comp <shader name>
```
