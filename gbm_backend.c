#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#include <gbm.h>

#include "gbm_backend.h"
#include "gl_debug.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

/* Global Variables */
struct gbm_device *gbm;
char device_path[256];
EGLDisplay egl_dpy;
EGLContext ctx;
EGLConfig cfg;
EGLint count;
int fd;

static const EGLint config_attribs[] = {
	EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
	EGL_NONE
};

static const char *egl_extension[] = {
		"EGL_KHR_create_context",
		"EGL_KHR_surfaceless_context"
};

/* Private function */
enum shader_type get_context(char *shader, int len){
	int i = 0;

	for (; shader[i] != '#' && i < len; i++);
	if (i == len)
		return TYPE_CORE;

	i += sizeof("#version xxx");
	if (strncmp(shader + i, "es", 2))
		return TYPE_CORE;
	else
		return TYPE_ES;
}

static EGLContext
create_context(EGLDisplay egl_dpy, EGLConfig cfg, enum shader_type type) {
	static const EGLint attribs[] = {
		EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR,
		EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR,
		EGL_CONTEXT_MAJOR_VERSION_KHR, 4,
		EGL_CONTEXT_MINOR_VERSION_KHR, 4,
		EGL_NONE
	};
	static const EGLint es_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	switch (type) {
	case TYPE_CORE: {
		eglBindAPI(EGL_OPENGL_API);
		EGLContext core_ctx = eglCreateContext(egl_dpy, cfg, EGL_NO_CONTEXT, attribs);

		if (core_ctx == EGL_NO_CONTEXT) {
			static const EGLint attribs_31[] = {
				EGL_CONTEXT_MAJOR_VERSION_KHR, 3,
				EGL_CONTEXT_MINOR_VERSION_KHR, 1,
				EGL_NONE
			};
			core_ctx = eglCreateContext(egl_dpy, cfg, EGL_NO_CONTEXT, attribs_31);
		}

		return core_ctx;
	}
	case TYPE_ES:
		eglBindAPI(EGL_OPENGL_ES_API);
		return eglCreateContext(egl_dpy, cfg, EGL_NO_CONTEXT, es_attribs);
	default:
		return NULL;
	}
}

/* Public functions */
int gbmInit(char* shader_code, int shader_size, char* shader_name) {
	enum shader_type context_type = get_context(shader_code, shader_size);
	int ret = -1;

	if (!epoxy_has_egl_extension(EGL_NO_DISPLAY, "EGL_MESA_platform_gbm")) {
		fprintf(stderr, "ERROR: Missing EGL_MESA_platform_gbm\n");
		goto out;
	}

	snprintf(device_path, sizeof(device_path), "/dev/dri/renderD%d", 129);

	fd = open(device_path, O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "ERROR: Couldn't open %s\n", device_path);
		goto out;
	}

	//
	// Display boilerplate
	//
	gbm = gbm_create_device(fd);
	if (gbm == NULL) {
		fprintf(stderr, "ERROR: Couldn't create gbm device\n");
		goto card_error;
	}

	egl_dpy = eglGetPlatformDisplayEXT(EGL_PLATFORM_GBM_MESA, gbm, NULL);
	if (egl_dpy == EGL_NO_DISPLAY) {
		fprintf(stderr, "ERROR: eglGetDisplay() failed\n");
		goto destroy_gbm_device;
	}

	if (!eglInitialize(egl_dpy, NULL, NULL)) {
		fprintf(stderr, "ERROR: eglInitialize() failed\n");
		goto destroy_gbm_device;
	}

	for (int i = 0; i < ARRAY_SIZE(egl_extension); i++) {
		if (!epoxy_has_egl_extension(egl_dpy, egl_extension[i])) {
			fprintf(stderr, "ERROR: Missing %s\n", egl_extension[i]);
			goto egl_terminate;
		}
	}

	if (!eglChooseConfig(egl_dpy, config_attribs, &cfg, 1, &count) || count == 0) {
		fprintf(stderr, "ERROR: eglChooseConfig() failed\n");
		goto egl_terminate;
	}

	ctx = create_context(egl_dpy, cfg, context_type);
    if (ctx == EGL_NO_CONTEXT) {
		fprintf(stderr, "ERROR: Fail to create a egl context\n");
		goto egl_terminate;
	}

	if (!eglMakeCurrent(egl_dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, ctx))
		goto destroy_context;

	if(!epoxy_has_gl_extension("GL_KHR_debug")) {
		fprintf(stderr, "ERROR: Missing GL_KHR_debug\n");
		goto destroy_context;
	}

	init_debug(shader_name);

	return 0;

destroy_context:
	eglDestroyContext(egl_dpy, ctx);
egl_terminate:
	eglTerminate(egl_dpy);
destroy_gbm_device:
	gbm_device_destroy(gbm);
card_error:
	close(fd);
out:
	fflush(stderr);
	return ret;
}

void gbmCleanup() {
	eglDestroyContext(egl_dpy, ctx);
	eglTerminate(egl_dpy);
	gbm_device_destroy(gbm);
	close(fd);
}

