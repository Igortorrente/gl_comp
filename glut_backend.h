#ifndef GLUT_BACKEND
#define GLUT_BACKEND

#include <stdio.h>
#include <assert.h>
#include <GL/freeglut.h>
#include <GL/glx.h>
#include "gl_debug.h"

#define init(shader_code, shader_size, shader_name) glut_backend_init(shader_name)
#define cleanup()
#define gl_specialize_shader specialize_shader

static PFNGLSPECIALIZESHADERPROC specialize_shader;

int glut_backend_init(char* shader_name) {
	int argc = 1;
	glutInit(&argc, &shader_name);
	glutInitWindowSize(800, 600);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitContextVersion(4, 4);
	glutCreateWindow("GL4 test");

	if(glutExtensionSupported("GL_KHR_debug")) {
		fprintf(stderr, "ERROR: Missing GL_KHR_debug\n");
		return -1;
	}

	init_debug(shader_name);

	specialize_shader = (PFNGLSPECIALIZESHADERPROC)glXGetProcAddress((unsigned char*)"glSpecializeShaderARB");
	if (!gl_specialize_shader) {
		fprintf(stderr, "failed to load glSpecializeShaderARB entry point\n");
		return -1;
	}

	return 0;
}

#endif //GLUT_BACKEND
